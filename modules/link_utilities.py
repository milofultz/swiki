import re

raw_start = '{{% raw %}}'
raw_end = '{{% endraw %}}'

re_wikilink = re.compile(r'{{.+?}}')
re_external_link = re.compile(r'<a href=".+?"')
re_special_characters = re.compile(r'[/()\'\".!?,]')
re_raw = re.compile(rf'({raw_start}.+?{raw_end})')


def kebabify(text: str) -> str:
    """ Format text to filename kebab-case """
    text = text[:200]  # Enforce max length of 200 chars
    text = re_special_characters.sub('', text)
    return text.replace(' ', '-').lower()


def get_local(content: str) -> list:
    """ Get list of all local link filenames """
    local_links = list()

    content_chunks = [
        chunk
        for chunk in re_raw.split(content)
        if not re_raw.match(chunk)
    ]
    for chunk in content_chunks:
        for match in re_wikilink.finditer(chunk):
            match_text = match.group()[2:-2]  # remove curly braces
            # filename if filename else text
            local_links.append(match_text.rsplit('|')[-1].strip())
    return local_links


def add_external(html: str) -> str:
    """ Modify all anchor tags for external links """
    def add_target_blank(match: re.Match):
        text = match.group()
        return f'{text} target="_blank"'
    return re_external_link.sub(add_target_blank, html)


def add_local(html: str) -> str:
    """ Replace all {{...|?...}} with anchor tags """
    # Get all text that is not enclosed in a raw block
    html_with_links = ''

    chunks = re_raw.split(html)
    for chunk in chunks:
        if raw_content_match := re_raw.match(chunk):
            raw_content = raw_content_match.groups()[0]
            html_with_links += raw_content[len(raw_start):-len(raw_end)]
        else:
            def make_link(match: re.Match):
                match_text = match.group()[2:-2].split('|')
                text = filename = match_text[0].strip()
                if len(match_text) == 2:
                    filename = match_text[1].strip()
                filename = kebabify(filename)
                return f'<a href="{filename}.html">{text}</a>'
            html_with_links += re_wikilink.sub(make_link, chunk)

    return html_with_links


def add_incoming_links(content: str, incoming_links: list) -> str:
    """ Add incoming links section to content """
    if not incoming_links:
        return content
    incoming_links_html = '<section id="incoming"><details open><summary>Incoming Links</summary><ul>'
    seen_incoming_links = set()
    incoming_links = sorted(
        incoming_links,
        key=lambda link: str.lower(link.get('title'))
    )
    for link in incoming_links:
        title, filename = link.get('title'), link.get('filename')
        if title not in seen_incoming_links:
            seen_incoming_links.add(title)
            incoming_links_html += f'<li><a href="{filename}.html">{title}</a></li>'
    incoming_links_html += '</ul></details></section>'
    return content + incoming_links_html
